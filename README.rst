This repo contains the Emacs Lisp package github-query, which provides a
function to retrieve, from within Emacs, the information of a GitHub issue.

Usage
=====

This package provides the function github-query-get-issue:

.. sourcecode:: Emacs-lisp

  (defun github-query-get-issue(owner repo issue-number)
    "Return issue ISSUE-NUMBER of GitHub repo OWNER/REPO.

  This function returns the response as an association list, but
  you can also use `github-query-get-attribute' for that."

The following unit test shows how to retrieve the information on the first
issue from repo `bbatsov/projectile`_ [1]_:

.. sourcecode:: Emacs-lisp

  (ert-deftest github-query--get-issue-retrieves-correct-response()
    (let ((response (github-query-get-issue "bbatsov" "projectile" 1)))
      (should (equal 1 (github-query-get-attribute 'number response)))
      (should (equal "Obey .gitignore, .bzrignore, etc."
                  (github-query-get-attribute 'title response)))
      (should (equal "https://github.com/bbatsov/projectile/issues/1"
                  (github-query-get-attribute 'html_url response)))))

To give an example use case, I use the command to insert an org-mode link to a
GitHub issue into the current buffer. The following piece of Lisp defines a
function that does that for a GitHub issue from bbatsov/projectile:

.. sourcecode:: Emacs-lisp

  (defun insert-projectile-issue-as-org-link(issue-number)
    (interactive "nIssue number: ")
    (insert-string (get-projectile-issue-as-org-link issue-number)))
   
  (defun get-projectile-issue-as-org-link(issue-number)
    (let ((response (github-query-get-issue "bbatsov" "projectile" issue-number)))
       (concat "[[" (github-query-get-attribute 'html_url response)
               "][" (github-query-get-attribute 'title response)
               "]]")))
   
Note that github-query-get-issue supports access to private repos using basic
authentication. To do so, you need to set `github-query-username' for the user
name and `github-query-password' for the password.

Installation
============

To use the package, first you have to retrieve it and then you have to tell
Emacs to load it. One way to retrieve the package is to clone the repo using
HTTPS::

  $> git clone https://swinkels@bitbucket.org/swinkels/github-query

or using SSH::
    
  $> git clone ssh://git@bitbucket.org/swinkels/github-query

Both commands create a local copy of the repo in subdirectory github-query.

Another way to retrieve the package is to download the latest development
version at https://bitbucket.org/swinkels/github-query/get/master.gz [2]_. The
command that uncompresses the archive::

  $> tar -xgzf master.gz

places the package in subdirectory swinkels-github-query-<commit-hash>, where
<commit-hash> specifies the last commit on the master branch.

To load the package, you can load github-query.el from your Emacs init.el, e.g.

.. sourcecode:: Emacs-lisp

  (load "~/.emacs.d/externals/sks-emacs-lisp/github-query.el")

Of course you should specify the directory where you placed github-query.

If you use Cask_ to maintain your Emacs configuration, you can also let Cask
create a link to the package so you can set the dependency in the Emacs Cask
file. The following snippet shows how to create the link::

    $> cd ~/.emacs.d
    $> cask link github-query <local-github-query-directory>

Then add the following statement to the Cask file for your Emacs
configuration:

.. sourcecode:: Emacs-lisp

   (depends-on "github-query")

Restart Emacs and you can use the package.

Development
===========

Technically, github-query uses Cask as-in "there is a Cask file". At this time
github-query has no runtime and development time dependencies that require
installation from packages that do not come with Emacs. So there is no need to
use Cask or even have Cask installed.

You can use the Makefile in the root of the repo to run the unit tests. To run
the unit tests just execute make::

  $> make

This should give you the following output::

   $> make
   cask exec emacs --no-site-file --no-site-lisp --batch -l tests/github-query-tests.el -f ert-run-tests-batch-and-exit
   Running 8 tests (2014-12-12 20:49:38+0100)
      passed  1/8  get-basic-authentication-headers-adds-correct-header
      passed  2/8  get-basic-authentication-headers-adds-no-authentication-header
   Contacting host: api.github.com:443
      passed  3/8  github-query--get-issue-handles-private-repos
   Contacting host: api.github.com:443
      passed  4/8  github-query--get-issue-kills-response-buffer
      passed  5/8  github-query--get-issue-retrieves-correct-response
      passed  6/8  github-query--stays-in-current-buffer
      passed  7/8  starts-with-handles-substrings-that-are-too-long
      passed  8/8  string-starts-with-given-string

   Ran 8 tests, 8 results as expected (2014-12-12 20:49:39+0100)

The unit tests themselves can be found in directory github-query/tests.

.. [1] I host my personal projects at Bitbucket so I had to choose an example repo that is not mine
.. [2] change the .gz extension to .zip or .bz2 for other formats

.. _bbatsov/projectile: https://github.com/bbatsov/projectile
.. _Cask: https://github.com/cask/cask

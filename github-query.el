;;; github-query.el --- Retrieves GitHub issue information from Emacs

;; Copyright (C) 2014 Pieter Swinkels

;; Author: Pieter Swinkels <swinkels.pieter@yahoo.com>
;; Keywords: github
;; Version: 0.0.1
;; Package-Requires:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:

;; This file provides a function to retrieve GitHub issue information.

(require 'json)

(defconst github-api-domain "api.github.com")

(defconst response-buffer-name-prefix (concat " *http " github-api-domain))

(defun github-query-get-issue(owner repo issue-number)
  "Return issue ISSUE-NUMBER of GitHub repo OWNER/REPO.

This function returns the response as an association list, but
you can also use `github-query-get-attribute' for that.

This function supports access to private repos using basic
authentication. To do so, you need to set `github-query-username'
for the user name and `github-query-password' for the password.
You can set these values locally, as shown in unit tests

  - `get-basic-authentication-headers-adds-correct-header'
  - `get-basic-authentication-headers-adds-no-authentication-header'"
  (let ((request-address (get-request-address owner repo issue-number)))
    (let ((url-request-extra-headers (get-basic-authentication-header)))
      (let ((response-buffer (url-retrieve-synchronously request-address)))
        (save-excursion
          (set-buffer response-buffer)
          (let ((response (json-read)))
            (kill-buffer response-buffer)
            response))))))

(defun get-credentials-for-private-repo()
  (let ((credentials-file "credentials-for-private-repo-test.json"))
    (when (file-exists-p credentials-file)
      (json-read-file credentials-file))))

(defun get-request-address(owner repo issue-number)
  (concat "https://" github-api-domain "/repos/" owner "/" repo "/issues/" (number-to-string issue-number)))

(defun get-basic-authentication-header()
  (if (boundp 'github-query-username)
      (let ((credentials (base64-encode-string (concat github-query-username ":" github-query-password))))
        `(("Authorization" . ,(concat "Basic " credentials))))))

(defun github-query-get-attribute(attribute response)
  (cdr (assoc attribute response)))

(defun get-response-buffers()
  (delq nil (mapcar 'as-response-buffer (buffer-list))))

(defun as-response-buffer(buffer)
  (when (starts-with (buffer-name buffer) response-buffer-name-prefix) buffer))

(defun starts-with(string starts-with-string)
  (when (>= (length string) (length starts-with-string))
    (equal (substring string 0 (length starts-with-string)) starts-with-string)))

(provide 'github-query)

;;; github-query.el ends here

(require 'ert)

;;add the directory that contains github-query.el to the `load-path' to be able
;;to require (and load) github-query.el
(let ((current-directory (file-name-directory load-file-name)))
  (setq github-query-root-path (expand-file-name ".." current-directory)))
(add-to-list 'load-path github-query-root-path)

(require 'github-query)

(ert-deftest github-query--get-issue-retrieves-correct-response()
  (let ((response (github-query-get-issue "bbatsov" "projectile" 1)))
    (should (equal 1 (github-query-get-attribute 'number response)))
    (should (equal "Obey .gitignore, .bzrignore, etc."
                (github-query-get-attribute 'title response)))
    (should (equal "https://github.com/bbatsov/projectile/issues/1"
                (github-query-get-attribute 'html_url response)))))

(ert-deftest github-query--get-issue-kills-response-buffer()
  (let ((response-buffers (get-response-buffers)))
    (github-query-get-issue "bbatsov" "projectile" 2)
    (should (equal response-buffers (get-response-buffers)))))

(ert-deftest github-query--get-issue-handles-private-repos()
  (let ((credentials (get-credentials-for-private-repo)))
    (when credentials
      (let ((get-value '(lambda (key) (cdr (assoc key credentials)))))
        (let ((github-query-username (funcall get-value 'username))
              (github-query-password (funcall get-value 'password))
              (owner        (funcall get-value 'owner))
              (repo         (funcall get-value 'repo))
              (issue-number (funcall get-value 'issue-number)))
          (let ((response (github-query-get-issue owner repo issue-number)))
            (should (equal issue-number (github-query-get-attribute 'number response)))))))))

(ert-deftest github-query--stays-in-current-buffer()
  (let ((buffer (current-buffer)))
    (github-query-get-issue "bbatsov" "projectile" 2)
    (should (equal buffer (current-buffer)))))

(ert-deftest get-basic-authentication-headers-adds-correct-header()
  (let ((github-query-username "hello") (github-query-password "world"))
    (let ((url-request-extra-headers (get-basic-authentication-header)))
      (should (equal (cdr (assoc "Authorization" url-request-extra-headers)) "Basic aGVsbG86d29ybGQ=")))))

(ert-deftest get-basic-authentication-headers-adds-no-authentication-header()
  (let ((url-request-extra-headers (get-basic-authentication-header)))
    (should (equal url-request-extra-headers nil))))

(ert-deftest string-starts-with-given-string()
  (should (starts-with " *http api.github.com:443*-283265" response-buffer-name-prefix)))

(ert-deftest starts-with-handles-substrings-that-are-too-long()
  (should (not (starts-with "*scratch*" response-buffer-name-prefix))))
